﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace pr5
{
    public partial class Form4 : Form
    {
        public int selectrow = -1;
        public string kodsot;
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {

            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string sql = "SELECT * FROM Сотрудники";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand MyCommand;
            string sql;
            string FIO = textBox1.Text.ToString();
            string Dol = textBox2.Text.ToString();

            sql = "INSERT INTO Сотрудники" + "(ФИО, Должность)" + "VALUES ('" + FIO + "','" + Dol + "')";
            MessageBox.Show(sql);
            MyCommand = new OleDbCommand(sql, connection);
            connection.Open();
            MyCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Сотрудники";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox1.Visible = false;
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            textBox1.Text = "";
            textBox2.Text = "";

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox3.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox4.Text = dataGridView1[2, selectrow].Value.ToString();
                label5.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox5.Text = textBox3.Text;
                kodsot = label5.Text;
            }
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
            groupBox1.Visible = false;
            groupBox3.Visible = false;
            textBox3.Text = "";
            textBox4.Text = "";
            

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                label5.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox3.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox5.Text = textBox3.Text;
                textBox4.Text = dataGridView1[2, selectrow].Value.ToString();
                kodsot = label5.Text;

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Выделите в сетке строку для редактирования");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            string FIO = textBox3.Text.ToString();
            string dol = textBox4.Text.ToString();
            string kod = label5.Text.ToString();
           

            sql = "UPDATE Сотрудники SET " + "ФИО = '" + FIO + "'," + "Должность = '" + dol + "'"  + "WHERE Код_сотрудника = "+ kod ;
            MessageBox.Show(sql);
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Сотрудники";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox2.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Выделите в сетке строку для редактирования");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;

            sql = "DELETE * From Сотрудники WHERE Код_сотрудника = " + kodsot;
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Сотрудники";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox5.Text = "";
            groupBox3.Visible = true;
            groupBox1.Visible = false;
            groupBox2.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите критерий поиска");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string znach = textBox6.Text.ToString();
            sql = "SELECT * FROM Сотрудники WHERE " + kriteriy + " Like " + " '" + znach + "' ";
            MessageBox.Show(sql);
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            sql = "SELECT * FROM Сотрудники";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            textBox6.Text = "";
            comboBox1.Text = "";
        }
    }
}
