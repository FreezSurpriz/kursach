﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace pr5
{
    public partial class Form3 : Form
    {
        public int selectrow = -1;
        public string KodTovara;
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            string sql = "SELECT * FROM Товары";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox10.Text = "";
            textBox9.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
            textBox6.Text = "";
            textBox11.Text = "";
            textBox13.Text = "";
            groupBox2.Visible = true;
            groupBox3.Visible = false;
            groupBox1.Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox13.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox12.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox4.Text = dataGridView1[3, selectrow].Value.ToString();
                textBox3.Text = dataGridView1[4, selectrow].Value.ToString();
                textBox2.Text = dataGridView1[5, selectrow].Value.ToString();
                textBox1.Text = dataGridView1[6, selectrow].Value.ToString();
                textBox14.Text = dataGridView1[1, selectrow].Value.ToString();
                KodTovara = dataGridView1[0, selectrow].Value.ToString();
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Выделите в сетке строку для редактирования");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            string KodTovara = textBox13.Text.ToString();
            string KodKat = textBox1.Text.ToString();
            string NazvanieTovara = textBox12.Text.ToString();
            string Marka = textBox5.Text.ToString();
            string Cina = textBox4.Text.ToString();
            string Cvet = textBox3.Text.ToString();
            string Probeg = textBox2.Text.ToString();
            
            sql = "UPDATE Товары SET " + "Код_кат = '" + KodKat + "'," + "Название_товара = '" + NazvanieTovara + "'," + "Марка = '" + Marka + "'," + "Цена = " + Cina + "," + "Цвет = '" + Cvet + "'," + "Пробег = '" + Probeg + "'"  + "WHERE Код_товра = " + KodTovara;
            MessageBox.Show(sql);
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Товары";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox1.Visible = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";

            selectrow = dataGridView1.CurrentCell.RowIndex;

            if (selectrow < (dataGridView1.RowCount - 1))
            {
                textBox13.Text = dataGridView1[0, selectrow].Value.ToString();
                textBox12.Text = dataGridView1[1, selectrow].Value.ToString();
                textBox5.Text = dataGridView1[2, selectrow].Value.ToString();
                textBox4.Text = dataGridView1[3, selectrow].Value.ToString();
                textBox3.Text = dataGridView1[4, selectrow].Value.ToString();
                textBox2.Text = dataGridView1[5, selectrow].Value.ToString();
                textBox1.Text = dataGridView1[6, selectrow].Value.ToString();
                textBox14.Text = dataGridView1[1, selectrow].Value.ToString();
                KodTovara = dataGridView1[0, selectrow].Value.ToString();

            }
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            groupBox1.Visible = true;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectrow == -1 || selectrow >= dataGridView1.RowCount - 1)
            {
                MessageBox.Show("Выделите в сетке строку для редактирования");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
         
            sql = "DELETE * From Товары WHERE Код_товра = " + KodTovara;
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Товары";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();

            groupBox3.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox14.Text = "";
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand MyCommand;
            string sql;
            string nazvaniye = textBox10.Text.ToString();
            string Marka = textBox9.Text.ToString();
            string Zena = textBox8.Text.ToString();
            string Cvet = textBox7.Text.ToString();
            string Probeg = textBox6.Text.ToString();
            string kod = textBox11.Text.ToString();

            sql = "INSERT INTO Товары" + "(Название_товара, Марка, Цена, Цвет, Пробег, Код_кат)" + "VALUES ('" + nazvaniye + "','" + Marka + "' , '" + Zena + "', '" + Cvet + "', '" + Probeg + "', '" + kod + "')";
            MessageBox.Show(sql);
            MyCommand = new OleDbCommand(sql, connection);
            connection.Open();
            MyCommand.ExecuteNonQuery();
            connection.Close();

            connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            sql = "SELECT * FROM Товары";
            OleDbCommand myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            groupBox2.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите критерий поиска");
                return;
            }
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            string kriteriy = comboBox1.SelectedItem.ToString();
            string znach = textBox15.Text.ToString();
            sql = "SELECT * FROM Товары WHERE " + kriteriy + " Like " + " '" + znach + "' ";
            MessageBox.Show(sql);
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source = 1.mdb";
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConnectionString;
            OleDbCommand myCommand;
            string sql;
            sql = "SELECT * FROM Товары";
            myCommand = new OleDbCommand(sql, connection);
            connection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds, "Результат");
            dataGridView1.DataSource = ds.Tables["Результат"].DefaultView;
            connection.Close();
            
        }

        private void поискДанныхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = -1;
            textBox15.Text = "";
            groupBox4.Visible = true;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
